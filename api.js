const $formPrincipal = document.getElementById('form-principal');
const PIPEDRIVE_URL = 'Pipedrive_Domain_URL';
const API_KEY = 'API_TOKEN';
$formPrincipal.addEventListener('submit', (e) => {
    e.preventDefault();
    const form = new FormData($formPrincipal);
    const payload = {
        'name': form.get('nombre'),
        'last_name': form.get('apellidos'),
        'email': form.get('correo'),
        'phone': form.get('telefono'),
        //using custom fields
        'e4021cec69f82dffad760cc4c6a7999092789597': form.get('tipo-documento'),
    };
    console.log(payload);
    fetch(`${PIPEDRIVE_URL}v1/persons?api_token=${API_KEY}`, {
        method: 'POST',
        body: JSON.stringify(payload),
        headers: {
            'Content-Type': "application/json",
            Authorization: 'Bearer ' + API_KEY
        }
    })
        .then(res => res.json())
        .then(res => { console.log(res) })
});